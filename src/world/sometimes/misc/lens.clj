(ns world.sometimes.misc.lens
  (:refer-clojure :exclude [get assoc update vals])
  (:require
   [world.sometimes.misc.lens.impl :as impl]))

(def at impl/lens)
(def get impl/lens-view)
(def update impl/lens-over)
(def assoc impl/lens-set)
(def vals impl/lens-mapped)

(defn at-path [k & ks]
  (reduce comp (at k) (map at ks)))
