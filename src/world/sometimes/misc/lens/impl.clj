(ns world.sometimes.misc.lens.impl)

(defprotocol Functor
  (-fmap [ftor f]))

(defn fmap [f ftor]
  (-fmap ftor f))

(deftype Identity [x]
  Functor
  (-fmap [ftor f]
    (Identity. (f x))))

(deftype Const [x]
  Functor
  (-fmap [ftor f]
    ftor))

(defn functor->val [ftor]
  (.x ftor))

(extend-protocol Functor
  clojure.lang.Seqable
  (-fmap [ftor f]
    (map f ftor))
  clojure.lang.PersistentVector
  (-fmap [v f]
    (mapv f v))
  clojure.lang.IPersistentMap
  (-fmap [m f]
    (reduce-kv (fn [result k v]
                 (assoc result
                        k
                        (f v)))
               nil
               m)))

(defn lens [k]
  (fn [->ftor]
    (fn [coll]
      (fmap
       (partial assoc coll k)
       (->ftor (get coll k))))))

(defn lens-view [l x]
  (->> x ((l ->Const)) functor->val))

(defn lens-over [l f x]
  (->> x ((l (comp ->Identity f))) functor->val))

(defn lens-set [l v x]
  (lens-over l (constantly v) x))

(defn lens-mapped [f]
  (fn [x]
    (->> x (fmap (comp functor->val f)) ->Identity)))
