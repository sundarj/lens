(ns world.sometimes.misc.lens-test
  (:require
   [clojure.test :refer [are deftest is]]
   [clojure.test.check.clojure-test :refer [defspec]]
   [clojure.test.check.generators :as gen]
   [clojure.test.check.properties :as prop]
   [world.sometimes.misc.lens.impl :refer [fmap
                                           Functor
                                           lens
                                           lens-set
                                           lens-mapped
                                           lens-over
                                           lens-view]]))

(defspec view-returns-the-value-at-key
  100
  (let [k :k]
    (prop/for-all [m (gen/hash-map k gen/any)]
      (= (k m)
         (lens-view (lens k) m)))))

(defspec set-replaces-the-value-at-key
  100
  (let [k :k]
    (prop/for-all [m (gen/hash-map k (gen/return :placeholder-value))
                   x gen/any]
      (= x
         (k (lens-set (lens k) x m))))))

(defspec over-applies-function-to-the-value-at-key
  100
  (let [k :k
        f inc]
    (prop/for-all [m (gen/hash-map k gen/int)]
      (= (update m k f)
         (lens-over (lens k) f m)))))

(deftest fmap-delegates-to-Functor-method
  (let [ftor (reify Functor
               (-fmap [ftor f]
                 (f 1)))]
    (is (= (inc 1)
           (fmap inc ftor)))))

(defspec mapped-applies-the-action-to-all-values
  100
  (let [f inc
        l lens-mapped]
    (prop/for-all [m (gen/hash-map :k gen/int)
                   x gen/any]
      (and (= m
              (lens-view l m))
           (= (fmap (constantly x) m)
              (lens-set l x m))
           (= (fmap f m)
              (lens-over l f m))))))

(defspec the-lens-laws-hold
  100
  (let [k :k
        l (lens k)]
    (prop/for-all [m (gen/hash-map k gen/int)
                   x gen/any
                   y gen/any]
      (and (= m
              (lens-set l (lens-view l m) m))
           (= x
              (lens-view l (lens-set l x m)))
           (= (lens-set l y m)
              (lens-set l y (lens-set l x m)))))))

(deftest composition
  (let [data {:k [1 2 3]}
        k (lens :k)
        idx (lens 0)]
    (are [x y] (= x y)
      (->> data
           (lens-view k)
           (lens-view idx))
      (lens-view (comp k
                       idx)
                 data)
      
      data
      (lens-view (comp k
                       lens-mapped)
                 data))))
